var apples = 0;
var people = 0;
var people_cost = 10;
var worker = 0;
var worker_cost = 100;
var gold = 0;
var gold_cost = 1000;
var robots = 0;
var robot_cost = 10;
var miner = 0;
var miner_cost = 100;

var apple_gain = 0;

var gold_gain = 0;

var game_started = 0;


$('#apple').click(function(){
	game_started = 1;

	apples++;

	$('#apple-count').html(apples);
});

$('#buy-person').click(function(){
	if(apples >= people_cost){
		people++;

		$('#people-count').html(people);

		apples-=people_cost;
		apples = Number(apples.toFixed(2));

		people_cost+=10;

		apple_gain++;
		apple_gain-=0.1;
		apple_gain = Number(apple_gain.toFixed(2));

		$('#apple-gain').html(apple_gain);

		$('#person-cost').html(people_cost);
		$('#apple-count').html(apples);
	} else {
		alert('You do not have enough apples to buy another person');
	}
});

$('#buy-worker').click(function(){
	if(apples >= worker_cost){
		worker++;

		$('#worker-count').html(worker);

		apples-=worker_cost;
		apples = Number(apples.toFixed(2));

		worker_cost+=100;

		apple_gain+=10;
		apple_gain--;
		apple_gain = Number(apple_gain.toFixed(2));

		$('#apple-gain').html(apple_gain);

		$('#worker-cost').html(worker_cost);
		$('#apple-count').html(apples);
	} else {
		alert('You do not have enough apples to buy another worker');
	}
});

$('#buy-gold').click(function(){
	if(apples >= gold_cost){
		gold++;

		$('#gold-count').html(gold);

		apples-=gold_cost;
		apples = Number(apples.toFixed(2));

		$('#gold-cost').html(gold_cost);
		$('#apple-count').html(apples);
	} else {
		alert('You do not have enough apples to buy another gold');
	}
});

$('#buy-robot').click(function(){
	if(gold >= robot_cost){
		robots++;

		$('#robot-count').html(robots);

		gold-=robot_cost;
		gold = Number(gold.toFixed(2));

		robot_cost+=10;

		apple_gain+=50;
		apple_gain = Number(apple_gain.toFixed(2));

		$('#apple-gain').html(apple_gain);

		$('#robot-cost').html(robot_cost);
		$('#gold-count').html(gold);
	} else {
		alert('You do not have enough gold to buy another robot');
	}
});

$('#buy-miner').click(function(){
	if(gold >= miner_cost){
		miner++;

		$('#miner-count').html(miner);

		gold-=miner_cost;
		gold = Number(gold.toFixed(2));

		miner_cost+=10;

		gold_gain+=0.1;

		apple_gain-=100;
		apple_gain = Number(apple_gain.toFixed(2));

		$('#apple-gain').html(apple_gain);

		$('#gold-gain').html(gold_gain);

		$('#miner-cost').html(miner_cost);
		$('#gold-count').html(gold);
	} else {
		alert('You do not have enough gold to buy another miner');
	}
});

$('#reset').click(function(){
	apples = 0;
	people = 0;
	people_cost = 10;
	worker = 0;
	worker_cost = 100;
	gold = 0;
	gold_cost = 1000;
	robots = 0;
	robot_cost = 10;
	miner = 0;
	miner_cost = 100;

	apple_gain = 0;

	gold_gain = 0;

	game_started = 0;

	$('#apple-count').html(apples);

	$('#people-count').html(people);
	$('#person-cost').html(people_cost);

	$('#worker-count').html(worker);
	$('#worker_cost').html(worker_cost);

	$('#gold-count').html(gold);
	$('#gold-cost').html(gold_cost);

	$('#robot-count').html(robots);
	$('#robot-cost').html(robot_cost);

	$('#miner-count').html(miner);
	$('#miner-cost').html(miner_cost);

	$('#apple-gain').html(apple_gain);

	$('#gold-gain').html(gold_gain);

	$('#reset-button').hide();
	$('applebutton').prop('disabled', false);
});

window.setInterval(function(){
	if(game_started == 1){
		apples+=apple_gain;
		apples = Number(apples.toFixed(2));

		$('#apple-count').html(apples);

		gold+=gold_gain;
		gold = Number(gold.toFixed(2));
		$('#gold-count').html(gold);

		if(apples <= 0){
			if(miner >= 1){
				miner--;
				alert('You don\'t have enough apples to feed your miners, 1 has died');
				$('#miner-count').html(miner);
			}
			else if(worker >= 1){
				worker--;
				alert('You don\'t have enough apples to feed your workers, 1 has died');
				$('#worker-count').html(worker);
			}
			else if(people >= 1){
				people--;
				alert('You don\'t have enough apples to feed your people, 1 has died');
				$('#people-count').html(people);
			} else {
				game_started = 0;
				alert('GAME OVER');
				$('applebutton').prop('disabled', true);
				$('#reset-button').show();
				$('#reset').prop('disabled', false);
			}
		}
	}
}, 1000);